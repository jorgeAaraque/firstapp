﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace endWeek
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Welcome to DOCTUS Tech Shop by George");
            Console.WriteLine("Please enter your name in the next line");
            string nameClient = Console.ReadLine();
            Console.WriteLine("Hello Mr. " + nameClient + " what kind of cell phone are you looking for?");
            Console.WriteLine("high or low range? " + "please write the range");
            Console.WriteLine("for example: high");
            Console.WriteLine("for example: low");
            string choiceRange = Console.ReadLine();

            clsCelular celular1;
            celular1 = new clsCelular();
            celular1.brand = "Doctus";
            celular1.name = "Semillero-V1-ULTRA";
            celular1.color = "black";
            celular1.weight = "150 gr";
            celular1.screen = "7 pulg";
            celular1.cam = "300 mpx";
            celular1.ram = "1 Tgb";

            clsCelular celular2;
            celular2 = new clsCelular();
            celular2.brand = "Doctus";
            celular2.name = "Semillero-V1-BETA";
            celular2.color = "White";
            celular2.weight = "350 gr";
            celular2.screen = "5 pulg";
            celular2.cam = "8 mpx";
            celular2.ram = "1 gb";

            clsCelular celular3plus;
            celular3plus = new clsCelular();
            celular3plus.brand = "Doctus";
            celular3plus.name = "Semillero-V1.0-plus";
            celular3plus.color = "Gold";
            celular3plus.weight = "75 gr";
            celular3plus.screen = "12 pulg (flexible)";
            celular3plus.cam = "800 mpx";
            celular3plus.ram = "3 Tgb";

            celular3plus cls = new celular3plus();
            cls.faceId = "Active" ;

            clsCelular celular4low;
            celular4low = new clsCelular();
            celular4low.brand = "Doctus";
            celular4low.name = "Semillero-V0.1. (pero funciona)";
            celular4low.color = "black";
            celular4low.weight = "500 gr";
            celular4low.screen = "4 pulg";
            celular4low.cam = "5 mpx";
            celular4low.ram = "0.5 gb";

            if (choiceRange == "high")
            {
                Console.WriteLine("Brand:" + celular1.brand);
                Console.WriteLine("Name:" + celular1.name);
                Console.WriteLine("Color:" + celular1.color);
                Console.WriteLine("Weight:" + celular1.weight);
                Console.WriteLine("Screen:" + celular1.screen);
                Console.WriteLine("Camera:" + celular1.cam);
                Console.WriteLine("Ram:" + celular1.ram);
            }
            else
            {
                Console.WriteLine("Brand:" + celular2.brand);
                Console.WriteLine("Name:" + celular2.name);
                Console.WriteLine("Color:" + celular2.color);
                Console.WriteLine("Weight:" + celular2.weight);
                Console.WriteLine("Screen:" + celular2.screen);
                Console.WriteLine("Camera:" + celular2.cam);
                Console.WriteLine("Ram:" + celular2.ram);
            };

            Console.WriteLine("Do you like the features of this cell phone?");
            Console.WriteLine("what to buy this phone or see the other range?");
            Console.WriteLine("write yes or not");
            string otherRange = Console.ReadLine();
            if ( otherRange == "yes")
            {
                Console.WriteLine("write range");
                Console.WriteLine("high+");
                Console.WriteLine("or");
                Console.WriteLine("beta low");
                string otherRangeTrue = Console.ReadLine();
                if (otherRangeTrue == "high+")
                {
                    Console.WriteLine("Brand:" + celular3plus.brand);
                    Console.WriteLine("Name:" + celular3plus.name);
                    Console.WriteLine("Color:" + celular3plus.color);
                    Console.WriteLine("Weight:" + celular3plus.weight);
                    Console.WriteLine("Screen:" + celular3plus.screen);
                    Console.WriteLine("Camera:" + celular3plus.cam);
                    Console.WriteLine("Ram:" + celular3plus.ram);
                    Console.WriteLine("face id: " + cls.faceId);
                }
                else
                {
                    Console.WriteLine("Brand:" + celular4low.brand);
                    Console.WriteLine("Name:" + celular4low.name);
                    Console.WriteLine("Color:" + celular4low.color);
                    Console.WriteLine("Weight:" + celular4low.weight);
                    Console.WriteLine("Screen:" + celular4low.screen);
                    Console.WriteLine("Camera:" + celular4low.cam);
                    Console.WriteLine("Ram:" + celular4low.ram);
                }
            }
            Console.WriteLine("Congratulations to completing the equipment review an advisor will contact you");
            Console.WriteLine("Thank you");
            Console.WriteLine("press Entre to exit.");
            Console.ReadLine();
        }
        
    }
}
