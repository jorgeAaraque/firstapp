﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace endWeek
{
    public class clsCelular
    {
        public clsCelular()
        {
            brand = "";
            name = "";
            color = "";
            weight = "";
            screen = "";
            cam = "";
            ram = "";
        }
        public string brand;
        public string name;
        public string color;
        public string weight;
        public string screen;
        public string cam;
        public string ram;
    }
}
